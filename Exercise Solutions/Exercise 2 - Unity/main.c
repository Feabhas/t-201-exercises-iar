#include "wms.h"

int main()
{
  motorInit();
  sevenSegInit();
  
  motorOn();
  sevenSegDisplay(1);
  
  return 0;
}
