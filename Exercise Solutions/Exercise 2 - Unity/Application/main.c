#include "motor.h"
#include "sevenseg.h"
#include "buttons.h"
#include "buzzer.h"

int main()
{
  int choice;
  
  motorInit();
  sevenSegInit();
  buzzerInit();
  doorInit();
  psInit();
  
  while(doorOpen())
  {
    buzzerOn();
  }
  buzzerOff();
  
  motorOn();
  
  for(;;)
  {
    choice = psGetValue();
    sevenSegDisplay(choice);
    motorChangeDir();
  }

  return 0;
}
