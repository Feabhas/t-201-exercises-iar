#include "unity.h"

#include "motor.h"
#include "sevenseg.h"
#include "buttons.h"
#include "buzzer.h"

#include "GPIORegisters.h"


#define SS_MASK     (bit16 | bit17 | bit18 | bit19)
#define MOTOR_MASK  (bit20 | bit21)
#define LATCH_MASK  (bit22)
#define BUZZER_MASK (bit23)

#define MASK        (SS_MASK | MOTOR_MASK | LATCH_MASK | BUZZER_MASK)

void setUp(void)
{
  IO1DIR &= ~(MASK);
  IO1CLR = MASK;
  motorInit();
  buzzerInit();
  doorInit();
  sevenSegInit();
  psInit(); 
}

void tearDown(void)
{
  IO1DIR &= ~(MASK);
  IO1CLR = MASK;
}

void testWMSInit(void)
{
  TEST_ASSERT_BITS_HIGH(MASK, IO1DIR);
  TEST_ASSERT_BITS_HIGH(SS_MASK, IO1PIN);  
  TEST_ASSERT_BITS_LOW(MOTOR_MASK, IO1PIN);  
  TEST_ASSERT_BIT_LOW(22, IO1PIN);
  TEST_ASSERT_BIT_LOW(23, IO1PIN);
}



void testSevenSegZero(void)
{
    sevenSegDisplay(0);
    TEST_ASSERT_BITS(SS_MASK, (0<<16), IO1PIN);
//  TEST_ASSERT_BITS_LOW(MOTOR_MASK, IO1PIN);  
//  TEST_ASSERT_BIT_LOW(22, IO1PIN);
//  TEST_ASSERT_BIT_LOW(23, IO1PIN);
}

void testSevenSegBlank(void)
{
    sevenSegDisplay(15);
    TEST_ASSERT_BITS(SS_MASK, SS_MASK, IO1PIN); 
//  TEST_ASSERT_BITS_LOW(MOTOR_MASK, IO1PIN);  
//  TEST_ASSERT_BIT_LOW(22, IO1PIN);
//  TEST_ASSERT_BIT_LOW(23, IO1PIN);
}

void testSevenSegEight(void)
{
    sevenSegDisplay(8);
    TEST_ASSERT_BITS(SS_MASK, (8<<16), IO1PIN);
//  TEST_ASSERT_BITS_LOW(MOTOR_MASK, IO1PIN);  
//  TEST_ASSERT_BIT_LOW(22, IO1PIN);
//  TEST_ASSERT_BIT_LOW(23, IO1PIN);
}


void testNegativeNumber(void)
{
  /* Any number less than zero display should be blanked*/
  sevenSegDisplay(-1);
  TEST_ASSERT_BITS(SS_MASK, SS_MASK, IO1PIN);
//  TEST_ASSERT_BITS_LOW(MOTOR_MASK, IO1PIN);  
//  TEST_ASSERT_BIT_LOW(22, IO1PIN);
//  TEST_ASSERT_BIT_LOW(23, IO1PIN);
}

void testGreaterThanMax(void)
{
  /* any number greater than 15 should display zero */
  sevenSegDisplay(16);
  TEST_ASSERT_BITS(SS_MASK, 0, IO1PIN);
//  TEST_ASSERT_BITS_LOW(MOTOR_MASK, IO1PIN);  
//  TEST_ASSERT_BIT_LOW(22, IO1PIN);
//  TEST_ASSERT_BIT_LOW(23, IO1PIN);
}


void testMotorOff(void)
{
  motorOn();
  motorOff();
  TEST_ASSERT_BIT_LOW(20, IO1PIN); 
//  TEST_ASSERT_BITS_HIGH(SS_MASK, IO1PIN);  
//  TEST_ASSERT_BIT_LOW(22, IO1PIN);
//  TEST_ASSERT_BIT_LOW(23, IO1PIN);
}


void testMotorOn(void)
{
  motorOff();
  motorOn();
  TEST_ASSERT_BIT_HIGH(20, IO1PIN); 
//  TEST_ASSERT_BITS_HIGH(SS_MASK, IO1PIN);  
//  TEST_ASSERT_BIT_LOW(22, IO1PIN);
//  TEST_ASSERT_BIT_LOW(23, IO1PIN);
}

void testMotorChangeDir(void)
{
  motorChangeDir();
  TEST_ASSERT_BIT_HIGH(21, IO1PIN); 
  motorChangeDir();
  TEST_ASSERT_BIT_LOW(21, IO1PIN); 
//  TEST_ASSERT_BITS_HIGH(SS_MASK, IO1PIN);  
//  TEST_ASSERT_BIT_LOW(22, IO1PIN);
//  TEST_ASSERT_BIT_LOW(23, IO1PIN);
}



