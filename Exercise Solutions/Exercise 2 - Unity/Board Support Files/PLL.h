#ifndef __PLL_H__
#define __PLL_H__
#include <stdint.h>

void initPLL(void);
uint32_t getprocessorClockFreq(void);
uint32_t getperipheralClockFreq(void);

#endif
