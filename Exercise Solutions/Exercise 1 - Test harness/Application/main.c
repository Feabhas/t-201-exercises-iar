#include "linkedList.h"
#include "BlockAllocator.h"
#include <stdio.h>

typedef int (*fnPtr)(void);

typedef struct 
{
  fnPtr thefunc;
  int   result;  
} TestStruct;


int testFunction(void)
{
  // Some code...
  puts("test function");
  return 1;  
}

static unsigned char pool[256];

int main()
{
  LinkedList_initialise();
  poolCreate(pool, 256, sizeof(TestStruct), 16);
  
  LIST list;
  LinkedList_create(&list);
  
  TestStruct *pNew = blockAllocate(pool);
  pNew->thefunc = testFunction;
  pNew->result  = 0;
  
  LinkedList_add(&list, (void*)pNew);
  
  Node *node = LinkedList_getFirst(&list);
  fnPtr fp;
  TestStruct *pStruct;
  
  while(node != NULL)
  {
    pStruct = (TestStruct*)(node->pData);
    fp = pStruct->thefunc;
    pStruct->result = fp();
    
    blockFree(pool, pStruct);
    
    node = LinkedList_getNext(&list);
  }
  
  LinkedList_empty(&list);
  
  return 0;
}
