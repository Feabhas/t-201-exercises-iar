#include "TestHarness.h"

// Test functions.  We could just have
// easily include their declarations in
// a header file.
//
extern TestResult testWillPass(void);
extern TestResult testWillFail(void);
extern void set_up(void);
extern void take_down(void);

int main(void)
{
  TestHarness_init();
  
  TestHarness_addTest("Will Pass", testWillPass);
  TestHarness_addTest("Will Fail", testWillFail);
  
  set_up();
  TestHarness_runTests();
  take_down();
  TestHarness_printResults();
  
  TestHarness_destroy();
}