//  Test cases

#include "TestHarness.h"
#include "UUT.h"

void set_up(void)
{
}

void take_down(void)
{
}

TestResult testWillPass(void)
{
  TestResult result;
  result = IS_EQUAL(givesCorrectResult(2, 3), 5);
  return result;
}


TestResult testWillFail(void)
{
  TestResult result;
  int a = 10;
  int b = 20;
  int expect = 30;
  int actual = givesIncorrectResult(a, b);
  result = IS_EQUAL(expect, actual);
  
  return result;
}