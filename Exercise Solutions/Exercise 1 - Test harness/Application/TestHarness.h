#ifndef TESTHARNESS_H
#define TESTHARNESS_H

typedef enum {FAIL, PASS} TestResult;
typedef TestResult (*testFunction)(void);

#define IS_EQUAL(exp, act)   (((exp) == (act))? PASS : FAIL)
#define IS_GREATER(exp, act) (((exp) > (act))? PASS : FAIL)
#define IS_LESS(exp, act)    (((exp) < (act))? PASS : FAIL)

void TestHarness_init();
void TestHarness_destroy();

__weak void set_up(void);
__weak void take_down(void);

void TestHarness_addTest(const char *desc, testFunction testfunc);
void TestHarness_runTests(void);
void TestHarness_printResults(void);

#endif // TESTHARNESS_H
