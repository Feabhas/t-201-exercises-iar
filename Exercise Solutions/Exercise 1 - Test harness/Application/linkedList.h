#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#include <stddef.h>

// ------------------------------------------------------------
// List node.  The node holds a void pointer
// that can (therefore) hold the address of
// any object the client requires.
//
typedef struct Node
{
  void *pData;
  struct Node* next;

} Node;

// Pointer to opaque type
//
typedef struct LinkedList *LIST;


// ------------------------------------------------------------
// Initialise the linked list
// mechanisms.  This function MUST
// be called before any list is created
// or used.
//
void LinkedList_initialise(void);

// ------------------------------------------------------------
// Create a new linked list.  You must explicitly
// create a list before you can add / get.
//
void LinkedList_create(LIST *const pList);

// ------------------------------------------------------------
// Add a new Node to the list.
// The list internally allocates its own
// memory for nodes using a fixed-block
// allocator.
//
void LinkedList_add(LIST *const pList, void *ptr);

// ------------------------------------------------------------
// Return the first node in the list
// Will return NULL if the list is empty
//
Node* LinkedList_getFirst(LIST *pList);

// ------------------------------------------------------------
// Get the next node in the list
// Will return NULL if there are no
// more nodes in the list.
//
Node* LinkedList_getNext (LIST *pList);

// ------------------------------------------------------------
// Clear the list and free up
// any allocated memory
//
void LinkedList_empty(LIST *pList);

#endif // LINKED_LIST_H