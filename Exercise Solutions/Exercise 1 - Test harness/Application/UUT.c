/*****************************************************************************/
/* UUT.c */

#include "UUT.h"
#include "TestHarness.h"

int givesIncorrectResult(int op1, int op2)
{
  return (op1 + op2 + 1);
}
 

int givesCorrectResult(int op1, int op2)
{
  return (op1 + op2);
}

