/*****************************************************************************/
/* TestHarness.c */

#include "TestHarness.h"
#include "linkedList.h"
#include "BlockAllocator.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

typedef struct
{
  const char*  description;
  testFunction test;
  TestResult   result;
  
}TestCase;

// For simplicity, stick to a single, static
// test suite.
//
#define TEST_POOL_SIZE        256
#define MAX_TEST_CASES        (TEST_POOL_SIZE / sizeof(TestCase))

#define DESCRIPTION_POOL_SIZE 128
#define DESCRIPTION_SIZE      (DESCRIPTION_POOL_SIZE/MAX_TEST_CASES)

static LIST testSuite;
static uint8_t testCasePool[TEST_POOL_SIZE];
static uint8_t descriptionPool[DESCRIPTION_POOL_SIZE];


void TestHarness_init()
{
  LinkedList_initialise();
  LinkedList_create(&testSuite);
  poolCreate(testCasePool, TEST_POOL_SIZE, sizeof(TestCase), MAX_TEST_CASES);
}


void TestHarness_addTest(const char *desc, testFunction testfunc)
{
  TestCase *pNewTest = blockAllocate(testCasePool);
  char *pDesc = blockAllocate(descriptionPool);
  strcpy(pDesc, desc);
  pNewTest->description = pDesc;
  pNewTest->test = testfunc;
  pNewTest->result = PASS;
  
  LinkedList_add(&testSuite, (void*)pNewTest);
}


void TestHarness_runTests(void)
{
  Node *node;
  TestCase *currentTest;
  node = LinkedList_getFirst(&testSuite);
  
  while(node != NULL)
  {
    currentTest = (TestCase*)node->pData;
    currentTest->result = currentTest->test();
    node = LinkedList_getNext(&testSuite);
  }
}

static const char* result_string[] = {"FAIL", "PASS" };

void TestHarness_printResults(void)
{
  Node *node;
  TestCase *currentTest;
  node = LinkedList_getFirst(&testSuite);
  
  puts("TEST RESULTS");
  puts("============");
  
  while(node != NULL)
  {
    currentTest = (TestCase*)node->pData;
    printf("%s : ", currentTest->description);
    printf("%s \n", result_string[currentTest->result]);
    
    node = LinkedList_getNext(&testSuite);
  }
}

void TestHarness_destroy()
{
  Node *node;
  TestCase *test;
  node = LinkedList_getFirst(&testSuite);
  
  while(node != NULL)
  {
    test = (TestCase*)node->pData;
    blockFree(testCasePool, test);
    blockFree(descriptionPool, (void*)test->description); 
    node = LinkedList_getNext(&testSuite);
  }
  LinkedList_empty(&testSuite);
}


__weak void set_up(void)
{
  // By default, do nothing
}

__weak void take_down(void)
{
  // By default, do nothing
}