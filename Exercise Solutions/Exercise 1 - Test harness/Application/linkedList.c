#include "linkedList.h"
#include "BlockAllocator.h"
#include <stdint.h>

typedef struct LinkedList
{
  Node *head;
  Node *current;
  
} LinkedList;


// List structures are allocated
// from one pool, the nodes from
// another.
//
#define LIST_POOL_SIZE 256
#define NODE_POOL_SIZE 1024
#define MAX_LISTS      (LIST_POOL_SIZE/sizeof(LinkedList))
#define MAX_NODES      (NODE_POOL_SIZE/sizeof(Node))

static uint8_t listPool[LIST_POOL_SIZE];
static uint8_t nodePool[NODE_POOL_SIZE];

void LinkedList_initialise(void)
{
  // Note you won't get MAX_LISTS or MAX_NODES elements
  // in the pool because of the overhead required by
  // the allocator.  Se la vie.
  //
  poolCreate(listPool, LIST_POOL_SIZE, sizeof(LinkedList), MAX_LISTS);
  poolCreate(nodePool, NODE_POOL_SIZE, sizeof(Node), MAX_NODES);
}


void LinkedList_create(LIST *const pList)
{
  LIST list = blockAllocate(listPool);
  list->head    = NULL;
  list->current = NULL;
  *pList = list;
}


void LinkedList_add(LIST *const pList, void *ptr)
{
  LIST list = *pList;
  
  Node *pNew = blockAllocate(nodePool);
  pNew->pData = ptr;
  pNew->next  = NULL;
  
  Node* iterator = list->head;
  if(iterator == NULL)
  {
    // Empty list
    list->head = pNew;
  }
  else
  {
    // Walk the list
    while(iterator->next != NULL)
    {
      iterator = iterator->next;
    }
    iterator->next = pNew;
  }
}


Node* LinkedList_getFirst(LIST *pList)
{
  LIST list = *pList;
  list->current = list->head;
  return list->current;
}


Node* LinkedList_getNext (LIST *pList)
{
  LIST list = *pList;
  Node *iterator = list->current->next;
  list->current = iterator;
  return iterator;
}


void LinkedList_empty(LIST *pList)
{
  LIST list = *pList;
  Node *toDelete;
  Node *iterator;
  
  iterator = list->head;
  while(iterator != NULL)
  {
    toDelete = iterator;
    iterator = iterator->next;
    blockFree(nodePool, toDelete);
  }
  
}