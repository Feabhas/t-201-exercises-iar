// Step tests

// Test framework headers
//
#include "unity.h"
#include "cmock.h"

// UUT headers
//
#include "steps.h"

// Mock object headers
//
#include "Mockmotor.h"
#include "Mocksevenseg.h"
#include "Mockbuzzer.h"
#include "Mockbuttons.h"


void setUp(void)
{
}

void tearDown(void)
{
}

void testWash(void)
{
  // Set expectations on mocks
  sevenSegDisplay_Expect(WASH);
  motorOff_Expect();
  motorChangeDir_Expect();
  motorOn_Expect();
  motorOff_Expect();
  motorChangeDir_Expect();
  motorOn_Expect();
  motorOff_Expect();
  motorChangeDir_Expect();
  motorOn_Expect();
  motorOff_Expect();
  motorChangeDir_Expect();
  motorOn_Expect();
  motorOff_Expect();
  
  // Verify step returns the correct result
  TEST_ASSERT_EQUAL(wash(FAST), WASH);
}


void testRinse(void)
{
  // Set expectations on mocks
  sevenSegDisplay_Expect(RINSE);
  
  for(int i = 0; i < NUM_RINSE_CYCLES; ++i)
  {
    motorOn_Expect();
    motorOff_Expect();
  }
  
  // Verify step returns the correct result
  TEST_ASSERT_EQUAL(rinse(FAST), RINSE);
}


void testComplete(void)
{
  // Set expectations on mocks
  sevenSegDisplay_Expect(COMPLETE);
  
  buzzerOn_Expect();
  buzzerOff_Expect();
  
  // Verify step returns the correct result
  TEST_ASSERT_EQUAL(complete(FAST), COMPLETE);
}


void testSpin(void)
{
  // Set expectations on mocks
  sevenSegDisplay_Expect(SPIN);
  
  motorOn_Expect();
  motorOff_Expect();
  
  // Verify step returns the correct result
  TEST_ASSERT_EQUAL(spin(FAST), SPIN);
}


void testDry(void)
{
  // Set expectations on mocks
  sevenSegDisplay_Expect(DRY);
  
  // Verify step returns the correct result
  TEST_ASSERT_EQUAL(dry(FAST), DRY);
}