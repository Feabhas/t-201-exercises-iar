#ifndef BUZZER_H
#define BUZZER_H

// Buzzer functions
//
void buzzerInit (void);
void buzzerOn (void);
void buzzerOff (void);


#endif // BUZZER_H