// Programme tests

// Test framework headers
//
#include "unity.h"
#include "cmock.h"

// UUT headers
//
#include "programmes.h"

// Mock object headers
//
#include "Mocksteps.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void testColourWash(void)
{
  // Set expectations
  fill_ExpectAndReturn(FAST, FILL);
  heat_ExpectAndReturn(FAST, HEAT);
  wash_ExpectAndReturn(FAST, WASH);
  empty_ExpectAndReturn(FAST, EMPTY);
  fill_ExpectAndReturn(FAST, FILL);
  rinse_ExpectAndReturn(FAST, RINSE);
  empty_ExpectAndReturn(FAST, EMPTY);
  spin_ExpectAndReturn(FAST, SPIN);
  dry_ExpectAndReturn(FAST, DRY);
  complete_ExpectAndReturn(FAST, COMPLETE);
  
  // Invoke Unit-Under-Test
  ColourWash();
}
