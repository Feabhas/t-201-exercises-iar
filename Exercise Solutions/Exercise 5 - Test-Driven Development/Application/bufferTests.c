// bufferTests.c

#include "unity.h"
#include "buffer.h"

void setUp(void)
{
}

void tearDown(void)
{
}

// 1. Test an empty buffer
void testIsEmpty(void)
{
  TEST_ASSERT_TRUE(isEmpty());
}

void testFlush(void)
{
  flush();
  TEST_ASSERT_TRUE(isEmpty());
}
void testAddSingle(void)
{
  Error e;
  int i;
  
  flush();
  i = 100;
  e = add(i);
  TEST_ASSERT_FALSE(isEmpty());
  TEST_ASSERT_EQUAL(OK, e);
}

// 3. Retrieve a stored item
void testGetSingle(void)
{
  Error e;
  int i;
  
  flush();
  add(100);
  
  e = get(&i);
  TEST_ASSERT_EQUAL(100, i);
  TEST_ASSERT_EQUAL(OK, e);
}

// 3. Attempt to retrieve from an empty buffer
void testGetEmpty(void)
{
  Error e;
  int i;
  
  flush();
  e = get(&i);
  TEST_ASSERT_EQUAL(EMPTY, e);
}

// Fill the buffer
void testFillBuffer(void)
{
  Error e;
  int i;
  
  flush();
  for (i = 0; i < 8; ++i)
  {
    e = add(i);
    TEST_ASSERT_EQUAL(OK, e);
  }
}

// Add one to a full buffer.
// Note: this test must follow
// testFillBuffer.
//
void testAddToFull(void)
{
  Error e;
  int i;
  
  i = 100;
  e = add(i);
  
  TEST_ASSERT_EQUAL(FULL, e);
}

// Read back the values from a 
// full buffer
void testReadFull(void)
{
  Error e;
  int i;
  int val;
  
  flush();
  for (i = 0; i < BUFFER_SIZE; ++i)
  {
    e = add(i);
    TEST_ASSERT_EQUAL(OK, e);
  }
  
  for (i = 0; i < BUFFER_SIZE; ++i)
  {
    e = get(&val);
    TEST_ASSERT_EQUAL(i, val);
    TEST_ASSERT_EQUAL(OK, e);
  }
}

// Alternate read and write many times
void testReadWriteAlternate(void)
{
  Error e;
  int i;
  int val;
  
  flush();
  for (i = 0; i < 2*BUFFER_SIZE; ++i)
  {
    e = add(i);
    TEST_ASSERT_EQUAL(OK, e);
    
    e = get(&val);
    TEST_ASSERT_EQUAL(val, i);
    TEST_ASSERT_EQUAL(OK, e);
  }
  
}

void testGetwithNullFromEmpty(void)
{
  Error e = OK;
  
  flush();
  e = get(0);
  
  // What to do?
  TEST_ASSERT_EQUAL(EMPTY, e);
}

void testGetwithNull(void)
{
  Error e;
  
  flush();
  add(100);
  
  e = get(0);
  TEST_ASSERT_EQUAL(OK, e);
}
