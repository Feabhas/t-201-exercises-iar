#ifndef BUFFER_H
#define BUFFER_H

typedef enum {OK, FULL, EMPTY} Error;

#define BUFFER_SIZE 8


Error add(int val);
//
// Adds a single item, val, to the buffer.
// If the item is stored the return value is OK.
// If you attempt to add an item to a full buffer
// the call fails and returns FULL.


Error get(int *const out);
//
// Returns the next item from the buffer.
// The value of the next item is placed in
// 'out'.
// If the buffer is empty the return value
// is set to EMPTY and the caller's value
// is unchanged.


int  isEmpty(void);
//
// Returns 1 (TRUE) if there are no items in the buffer; 
// else 0 (FALSE)


void flush(void);
//
// Empties the buffer of its contents and resets it.

#endif // BUFFER_H