// Buffer.c

#include "buffer.h"
#include <stddef.h>

static int count = 0;
static int container[BUFFER_SIZE];
static int write = 0;
static int read = 0;

Error add(int val)
{
  if(count == BUFFER_SIZE)
  {
    return FULL;
  }
  
  container[write] = val;
  ++count;
  ++write;
  if (write == BUFFER_SIZE) write = 0;
  
  return OK;
}

Error get(int *const out)
{  
  if (count == 0)
  {
    return EMPTY;
  }
  else
  {
    if(out != NULL)
    {  
      *out = container[read];
      ++read;
      --count;
      if (read == BUFFER_SIZE) read = 0;
    }
  }
  return OK;
}

int  isEmpty(void)
{
  return(count == 0);
}

void flush(void)
{
  count = 0;
  read = 0;
  write = 0;
}