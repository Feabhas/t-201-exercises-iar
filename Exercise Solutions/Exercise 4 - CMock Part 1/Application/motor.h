#ifndef MOTOR_H
#define MOTOR_H

// Motor control functions
//
void motorInit (void);
void motorOn (void);
void motorOff (void);
void motorChangeDir (void);

#endif // MOTOR_H

