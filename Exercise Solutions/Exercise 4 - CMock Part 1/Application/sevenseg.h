#ifndef SEVENSEG_H
#define SEVENSEG_H

// Seven-Segment display functions
//
void sevenSegInit (void);
void sevenSegDisplay (int n);
void sevenSegBlank (void);

#endif // SEVENSEG_H