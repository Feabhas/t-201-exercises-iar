#ifndef PROGRAMMES_H
#define PROGRAMMES_H

/*----------------------------------------------------------------------------*/
/*
Behaviour:
  A wash programme iterates through a sequence of wash steps.
  The sequence of steps is given in the WMS Case study.

  ProgInit() must be called (only once) before the first 
  Wash Programme is called.

Inputs:
  None.

Outputs:
  None
*/
/*----------------------------------------------------------------------------*/
void ProgInit   (void);

void ColourWash (void);
void WhiteWash  (void);
void MixedWash  (void);
void EconomyWash(void);
void Prog1Wash  (void);
void Prog2Wash  (void);


#endif // PROGRAMMES_H