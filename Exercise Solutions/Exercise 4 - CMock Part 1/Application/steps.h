#ifndef STEPS_H
#define STEPS_H

/*----------------------------------------------------------------------------*/
/*
Behaviour:
    Each step outputs its step number to the 7-segment display
   
    The "Spin" step simply drives the disk for a short period.
    
    The "Wash" step does the following:
        - rotate the disk in one direction for a period
        - pause for a short time
        - rotate the disk in the other direction for the rotation period
        - pause for the test period
        - repeat this cycle for a number of iterations
    
    
    The "Rinse" step does the following:
         - rotate the disk in one direction for a period
         - pause for a short time
         - repeat this cycle for a number of iterations

    The "Complete" step sounds the buzzer briefly
    
    The "Fill" step, the first time it is called it checks the door is closed. 
    If it is open the buzzer is sounded for a brief period. 

Inputs:
    period - how long to perform the step

Outputs:
    The step performed
*/
/*----------------------------------------------------------------------------*/

typedef enum 
{
  EMPTY = 1, 
  FILL, 
  HEAT, 
  WASH, 
  RINSE, 
  SPIN, 
  DRY, 
  COMPLETE

} ProgrammeStep;

typedef unsigned long duration_msec;

#define NUM_WASH_CYCLES   4
#define NUM_RINSE_CYCLES  4
#define FAST              (duration_msec)500
#define MEDIUM            (duration_msec)1000
#define SLOW              (duration_msec)2000


ProgrammeStep empty   (duration_msec period);
ProgrammeStep fill    (duration_msec period);
ProgrammeStep heat    (duration_msec period);
ProgrammeStep wash    (duration_msec period);
ProgrammeStep rinse   (duration_msec period);
ProgrammeStep spin    (duration_msec period);
ProgrammeStep dry     (duration_msec period);
ProgrammeStep complete(duration_msec period);



#endif // STEPS_H
