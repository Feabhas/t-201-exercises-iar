#include "sevenseg.h"
#include "GPIORegisters.h"

#define SEVENSEG_BITS (LED1 | LED2 | LED3 | LED4)

void sevenSegInit (void)
{
  IO1DIR |= SEVENSEG_BITS;
  sevenSegBlank();
}
  
void sevenSegDisplay (int n)
{
#ifdef NO_BUGS
  if((n < 0) || (n > 15))
  {
    sevenSegBlank();
  }
  else if((n > 9) && (n < 15))
  {
    n = 0;
  }
  IO1CLR = SEVENSEG_BITS;
  IO1SET = (n << 16);

#else
  IO1SET = (n << 16);
#endif  
  
}

void sevenSegBlank (void)
{
  /* Display blank (15) */
  IO1SET = SEVENSEG_BITS;
}  