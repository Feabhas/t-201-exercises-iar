#include "programmes.h"
#include "steps.h"
#include "buzzer.h"
#include "sevenseg.h"
#include "motor.h"
#include "buttons.h"

#define RATE FAST

void ProgInit   (void)
{
  buzzerInit();
  motorInit();
  sevenSegInit();
  doorInit();
  psInit();
}

void ColourWash (void)
{
  fill(RATE);
  heat(RATE);
  wash(RATE);
  empty(RATE);
  fill(RATE);
  rinse(RATE);
  empty(RATE);
  spin(RATE);
  dry(RATE);
  complete(RATE);
}

void WhiteWash(void)
{
  fill(RATE);
  wash(RATE);
  empty(RATE);
  fill(RATE);
  heat(RATE);
  wash(RATE);
  empty(RATE);
  fill(RATE);
  rinse(RATE);
  empty(RATE);
  spin(RATE);
  dry(RATE);
  complete(RATE);
}


void MixedWash(void)
{
  fill(RATE);
  heat(RATE);
  wash(RATE);
  empty(RATE);
  fill(RATE);
  rinse(RATE);
  empty(RATE);
  fill(RATE);
  rinse(RATE);
  empty(RATE);
  spin(RATE);
  dry(RATE);
  complete(RATE);
}

void EconomyWash(void)
{
  fill(RATE);
  heat(RATE);
  wash(RATE);
  empty(RATE);
  fill(RATE);
  rinse(RATE);
  empty(RATE);
  spin(RATE);
  dry(RATE);
  complete(RATE);
}

void Prog1Wash(void)
{
}

void Prog2Wash(void)
{
}