#include "buzzer.h"
#include "GPIORegisters.h"

#define BUZZER_MASK       (bit23)

void buzzerInit (void)
{
  IO1DIR |= BUZZER_MASK;
  buzzerOff();
}

void buzzerOn(void)
{
  IO1SET = BUZZER_MASK;
}

void buzzerOff(void)
{
  IO1CLR = BUZZER_MASK;
}