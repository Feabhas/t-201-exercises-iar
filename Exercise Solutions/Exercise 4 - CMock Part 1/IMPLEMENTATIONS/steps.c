#include "steps.h"
#include "motor.h"
#include "sevenseg.h"
#include "buzzer.h"

// Software wait function
//
#define COUNT_PER_MSEC 2000

static void wait(duration_msec delay)
{
   volatile duration_msec time = delay * COUNT_PER_MSEC;
   for (; time > 0 ; --time);
}


ProgrammeStep wash(duration_msec period)
{
  int i;
  
  sevenSegDisplay(WASH);
  for(i = 0; i < NUM_WASH_CYCLES; ++i)
  {
    motorOff();
    motorChangeDir();  
    wait(period);
    motorOn();
    wait(period);
  }
  motorOff();
  return WASH;
}


ProgrammeStep empty(duration_msec period)
{
  sevenSegDisplay(EMPTY);
  wait(period);
  return EMPTY;
}


ProgrammeStep fill(duration_msec period)
{
  sevenSegDisplay(FILL);
  wait(period);
  return FILL;
}


ProgrammeStep heat(duration_msec period)
{
  sevenSegDisplay(HEAT);
  wait(period);
  return HEAT;
}


ProgrammeStep rinse(duration_msec period)
{
  int i;
  
  sevenSegDisplay(RINSE);
  for(i = 0; i < NUM_RINSE_CYCLES; ++i)
  {
    motorOn(); 
    wait(period);
    motorOff();
    wait(period);
  }
  return RINSE;
}


ProgrammeStep spin(duration_msec period)
{
  sevenSegDisplay(SPIN);
  motorOn();
  wait(period);
  motorOff();
  return EMPTY;
}


ProgrammeStep dry(duration_msec period)
{
  sevenSegDisplay(DRY);
  wait(period);
  return DRY;
}


ProgrammeStep complete(duration_msec period)
{
  sevenSegDisplay(COMPLETE);
  buzzerOn();
  wait(500);
  buzzerOff();
  return EMPTY;
}

