#include "programmes.h"
#include "steps.h"
#include "buzzer.h"
#include "sevenseg.h"
#include "motor.h"
#include "buttons.h"

#define RATE FAST

void ProgInit   (void)
{
  buzzerInit();
  motorInit();
  sevenSegInit();
  doorInit();
  psInit();
}

void ColourWash (void)
{
  // -------------------------------------
  // TO DO:
  // Implement the colour wash...
  //
  // -------------------------------------
}

void WhiteWash(void)
{
  // -------------------------------------
  // OPTIONAL:
  // Implement the white wash...
  //
  // -------------------------------------
}


void MixedWash(void)
{
  // -------------------------------------
  // OPTIONAL:
  // Implement the mixed wash...
  //
  // -------------------------------------
}

void EconomyWash(void)
{
  // -------------------------------------
  // OPTIONAL:
  // Implement the economy wash...
  //
  // -------------------------------------
}

void Prog1Wash(void)
{
}

void Prog2Wash(void)
{
}