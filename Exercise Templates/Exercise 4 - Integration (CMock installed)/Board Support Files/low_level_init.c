/**************************************************
 *
 * This module contains the function `__low_level_init', a function
 * that is called before the `main' function of the program.  Normally
 * low-level initializations - such as setting the prefered interrupt
 * level or setting the watchdog - can be performed here.
 *
 * Note that this function is called before the data segments are
 * initialized, this means that this function cannot rely on the
 * values of global or static variables.
 *
 * When this function returns zero, the startup code will inhibit the
 * initialization of the data segments. The result is faster startup,
 * the drawback is that neither global nor static data will be
 * initialized.
 *
 * Copyright 1999-2004 IAR Systems. All rights reserved.
 *
 * $Revision: 21623 $
 *
 **************************************************/
#include <stdint.h>
#include <intrinsics.h>
#include <NXP/iolpc2129.h>
#include "PLL.h"

#ifdef __cplusplus
extern "C" {
#endif

#pragma language=extended
  
void CPUinit(void);
void INTERRUPTSinit(void);

__irq __arm void IRQ_Handler(void);
__interwork int __low_level_init(void);

__interwork int __low_level_init(void)
{
  /*==================================*/
  /*  Initialize hardware.            */
  /*==================================*/
  CPUinit();
   __disable_interrupt();
  INTERRUPTSinit();
  __enable_interrupt();

  /*==================================*/
  /* Choose if segment initialization */
  /* should be done or not.           */
  /* Return: 0 to omit seg_init       */
  /*         1 to run seg_init        */
  /*==================================*/
  return 1;
}

/*-------------------------------------------------------------------------------------------------------------------------------*/
static void DefDummyInterrupt(void)
{
}


/*-------------------------------------------------------------------------------------------------------------------------------*/
void CPUinit(void)
{
 
  initPLL();

  //Init MAM & Flash memory fetch
  MAMCR_bit.MODECTRL=2;   // MAM fully enabled
  MAMTIM_bit.CYCLES=4;    // fetch cycles are 4 processor clocks (cclk) in duration

  //GPIO init
  PINSEL0=0x00050005; //P0 enable UART0 and UART1 Tx/Rx pins
  PINSEL1=0;          //P0 upper 16 bits all GPIO
}


/*-------------------------------------------------------------------------------------------------------------------------------*/
//INTERRUPT initialisation and handling functions
/*-------------------------------------------------------------------------------------------------------------------------------*/
void INTERRUPTSinit(void)
{
#ifdef SRAM_VIA_JLINK
  MEMMAP = 2;   // remap IVT to RAM
#endif
  
  // Setup interrupt controller.
  VICProtection = 0U;

  // Disable ALL interrupts
  VICIntEnClear = 0xffffffff;
  VICDefVectAddr = (uint32_t)&DefDummyInterrupt;
}

/*-------------------------------------------------------------------------------------------------------------------------------*/
// not used in this project
__irq __arm void IRQ_Handler(void)
{
  void (*interrupt_function)(void);
  uint16_t vector;

  vector = VICVectAddr;                     // GET VECTOR ADDRESS FROM VIC CONTROLLER
  interrupt_function = (void(*)(void))vector;

  (*interrupt_function)();                  // Call vectored interrupt function.

  VICVectAddr = 0U;                          // Clear interrupt in VIC.
}

#pragma language=default

#ifdef __cplusplus
}
#endif
