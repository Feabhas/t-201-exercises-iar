// -----------------------------------------------------------------------------
//  Unit-Under-Test
//
// You can use this code to make sure your
// test harness is working correctly.
//
// -----------------------------------------------------------------------------
#ifndef UUT_H
#define UUT_H

int givesIncorrectResult(int op1, int op2);
/*
  Description:
  Adds the two operands and returns the result
	
  Parameters:
  (IN)  op1 - left hand operand
  (IN)  op2 - right hand operand
 
  Returns:
  op1 + op2

  Pre-conditions:
  None.

  Post-Conditions:
  This version of the function fails - that is, it does not
  return the correct value.  The returned value will be 1
  greater than expected.
*/


int givesCorrectResult(int op1, int op2);
/*
  Description:
  Adds the two operands and returns the result
	
  Parameters:
  (IN)  op1 - left hand operand
  (IN)  op2 - right hand operand
 
  Returns:
  op1 + op2

  Pre-conditions:
  None.

  Post-Conditions:
  This version of the function returns the correct value.  
*/


#endif /* UUT_H */
