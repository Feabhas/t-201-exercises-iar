// -----------------------------------------------------------------------------
//  Unit-Under-Test
//
// You can use this code to make sure your
// test harness is working correctly.
//
// -----------------------------------------------------------------------------

#include "UUT.h"
#include "TestHarness.h"

int givesIncorrectResult(int op1, int op2)
{
  return (op1 + op2 + 1);
}
 

int givesCorrectResult(int op1, int op2)
{
  return (op1 + op2);
}

