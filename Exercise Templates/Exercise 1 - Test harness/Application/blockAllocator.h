/*****************************************************************

  AC-401

  fixed-block memory allocator.

  In this example the client must allocate a region
  of memory within which the allocator will work.
  
  All the working structures for the allocator are
  contained in the memory pool, so the number of
  blocks that can be allocated will be less than the
  maximum.

  Typical overhead is 8 bytes per block, plus a one-off
  16 bytes for allocator management.

  So, for an allocator that must hold 32, 16-byte blocks
  the pool must be:

  32 * (16 + 8) + 16 = 784 bytes, minimum

  The allocator will word-align non-standard size blocks.

  NOTE:
  This allocator has not been designed to be particularly
  robust or thread-safe.

******************************************************************/

#ifndef BLOCKALLOCATOR_H
#define BLOCKALLOCATOR_H

/*
  Create the pool, from the supplied memory region.

  pre:  pPool must be allocated
        poolSize = sizeof(*pPool) (in bytes)
  
  post: Block allocator constructed.
        
  ret:  Actual number of blocks allocated

  notes:
        Block size will always be extended to the
        nearest word boundary block size.

        maxBlocks is a hint to the allocator.  If
        there is not enough space for maxBlocks
        blocks the allocator will create as many
        as it can in the space provided (see above)
*/
unsigned int poolCreate (
                         void* pPool, 
                         const unsigned int poolSize, 
                         const unsigned int blockSize, 
                         const unsigned int maxBlocks
                         );

/*
  Allocate a block from the pool/

  pre:  Allocator must be created (poolCreate())
  
  post: Available blocks decremented
        
  ret:  Address of block;
        0 if no more blocks are available

  notes:
        Although the block returned will be at
        least as big as the blockSize, there is
        no boundary checking.  Writing beyond
        the size of the block may well corrupt
        other program objects (but should not
        affect the allocator operation)

*/
void* blockAllocate(void *pPool);


/*
  Return a block to the pool/

  pre:  At least one block allocated from the
        supplied pool.
  
  post: Block returned to available use
        
  ret:  

  notes:
        De-allocating a null pointer has
        no effect.

        Returning a block from another pool
        will (likely) corrupt the supplied
        pool's operation.  So don't.
*/
void blockFree(void *pPool, void *pBlock);


#endif /* BLOCKALLOCATOR_H */