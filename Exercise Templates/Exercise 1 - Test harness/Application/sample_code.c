// -----------------------------------------------------------------------------
// Sample code
//
// This code shows the basic operation of the Linked List
// and Block Memory Allocator.
// It also shows how to iterate through a list.
//
// -----------------------------------------------------------------------------

#include "linkedList.h"
#include "BlockAllocator.h"

typedef int (*fnPtr)(void);

typedef struct 
{
  fnPtr thefunc;
  int   result;  
} TestStruct;

int testFunction(void)
{
  // Some code...
  return 1;  
}

// -------------------------------------------------------------------------------
static unsigned char pool[256];                  // Static memory for the block
                                                 // allocator to use.
int main()
{
  // -----------------------------------------------------------------------------
  LinkedList_initialise();                       // You must initialise the list
  poolCreate(pool, 256, sizeof(TestStruct), 16); // and pool before use.
  
  // -----------------------------------------------------------------------------
  LIST list;                                     // Create the list.
  LinkedList_create(&list);
  
  // -----------------------------------------------------------------------------
  TestStruct *pNew = blockAllocate(pool);        // Allocate a new TestStruct from
  pNew->thefunc = testFunction;                  // the pool and initialise it.
  pNew->result  = 0;                             // Then, add it to the list.
  LinkedList_add(&list, (void*)pNew);
  
  // -----------------------------------------------------------------------------
  Node *node = LinkedList_getFirst(&list);       // Iterate through the list.  The
  fnPtr fp;			                 // 'node' variable is our
  TestStruct *pStruct;                           // iterator.
                                                 // The �getFirst() and �getNext() 
  while(node != NULL)                            // functions return pointers to
  {                                              // list nodes.  The pointer will
    pStruct = (TestStruct*)(node->pData);        // be NULL at the end of the
    fp = pStruct->thefunc;                       // list.
    pStruct->result = fp();
    
    // ---------------------------------------------------------------------------
    blockFree(pool, pStruct);                    // De-allocate the memory for the 
                                                 // current TestStruct.
    node = LinkedList_getNext(&list);            // NOTE:  You must manage the 
  }                                              // memory of the items you put in
                                                 // the list yourself!
  // -----------------------------------------------------------------------------
  LinkedList_empty(&list);                       // Empty the list and free up its
                                                 // nodes.
  return 0;
}?
