/*****************************************************************

  AC-401

  fixed-block memory allocator.


  The allocator is organised as follows:
              
   ------------------------------------------------------------------
  | HDR | BH | BH | BH | BH |  BLOCK  |  BLOCK  |  BLOCK  |  BLOCK  |
  -------------------------------------------------------------------
  
  The header holds configuration information for the allocator:
  - Start of the block handles
  - Start of the block area
  - Size of the blocks
  - etc.
  
  The block handles are used to manage allocation / deallocation.  No
  memory is moved - only pointer manipulation.  Each block points to
  its own (data) block.
  
                         ---------        ---------                          
                         | BLOCK |        | BLOCK |
                         ---------        --------- 
                             ^                ^
                             |                |
   POOL HEADER           ---------       ---------- 
  ----------------       | pBlock |      | pBlock |
  | pCurrentFree |  ---> | pNext  | ---> | pNext  |---> 0
  ----------------       ----------      ----------
         ^              BLOCK HANDLE    BLOCK HANDLE
         |
      pHeader
  
  The header always points to the next free block (handle).
  
  De-allocated blocks are always returned to the head of the list.
  
************************************************************************/

#include "BlockAllocator.h"
#include <stdint.h>
#include <string.h>


/*
  Block Handles form the allocator's
  'free list'.  Each block points
  to its own data block, and to the
  next free block in the list.
*/

typedef struct BlockHandle_t
{
  void  *pBlock;
  struct BlockHandle_t* pNext;
    
} BlockHandle;


/*
  PoolHeader contains basic pool management
  information.
*/
typedef struct PoolHeader_t
{
  BlockHandle *pCurrentFree;
  BlockHandle *BlockHandles;
  void        *pBlockStart;
  unsigned int blockSize;
  
} PoolHeader;


/* HELPER FUNCTION DECLARATIONS
*/

unsigned int wordAlignBlock (const unsigned int blockSize);

unsigned int calculateNumBlocks (
                                 const unsigned int poolSize, 
                                 const unsigned int blockSize, 
                                 const unsigned int maxBlocks
                                 );

BlockHandle* block_getBlockHandle(void *pPool, void *pBlock);


#define WORD_SIZE     4
#define NOT_USED      0xA5
#define READY_FOR_USE 0xFF





unsigned int poolCreate (
                         void* pPool, 
                         const unsigned int poolSize, 
                         const unsigned int blockSize, 
                         const unsigned int maxBlocks
                         )
{
  PoolHeader *pHeader = (PoolHeader*)pPool;
  void *pBlockIterator;
  unsigned int numBlocks;
  unsigned int i;
 
  pHeader->blockSize = wordAlignBlock(blockSize);
  numBlocks = calculateNumBlocks(poolSize, pHeader->blockSize, maxBlocks);
  
  pHeader->BlockHandles = (BlockHandle*)((uint32_t)pPool + sizeof(PoolHeader));
  pHeader->pCurrentFree = pHeader->BlockHandles;
  pHeader->pBlockStart = (void*)((unsigned int)(pHeader->BlockHandles) + (numBlocks * sizeof(BlockHandle)));
  
  
  /* 
  Construct the initial allocator free list.  On construction the first block handle is
  the current free block; and each block handle points to the next, like a daisy-chain.
  */
  pBlockIterator = pHeader->pBlockStart;
  
  for (i = 0; i < numBlocks; ++i)
  {
    pHeader->BlockHandles[i].pBlock = pBlockIterator;
    pHeader->BlockHandles[i].pNext  = &pHeader->BlockHandles[i+1];
      
    pBlockIterator = (void*)((unsigned int)pBlockIterator + pHeader->blockSize);
  }
  pHeader->BlockHandles[numBlocks-1].pNext = 0; /* Terminate the list */
  
  
  return numBlocks; /* How many blocks were actually allocated? */
}


void* blockAllocate(void *pPool)
{
  PoolHeader *pHeader = (PoolHeader*)pPool;
  BlockHandle *pAllocatedHandle;
  void  *pAllocatedBlock;
  
  if (pHeader->pCurrentFree == 0)
  {
    /*
    If the currentFree pointer is null it
    means there are no more free blocks.
    Indicate this to the client by returning
    a null pointer (as with malloc())
    */
    pAllocatedBlock = 0;
  }
  else
  {
    /*
    Allocate the memory and point 
    the currentFree pointer to the 
    next available block (handle)
    */
    pAllocatedHandle = pHeader->pCurrentFree;
    pAllocatedBlock = pAllocatedHandle->pBlock;
    pHeader->pCurrentFree = pAllocatedHandle->pNext;
    
    /*
    Mark the block memory as READY.  This allows a 
    quick visual check when debugging.
    */
    memset(pAllocatedBlock, READY_FOR_USE, pHeader->blockSize);
  }

  return pAllocatedBlock;
}


void blockFree(void *pPool, void *pBlockToFree)
{
   PoolHeader *pHeader = (PoolHeader*)pPool;
   BlockHandle *pHandleToFree;
   
   if (pBlockToFree == 0)
   {
     /* Can't do anything with a null pointer */
     return;
   }
   
   pHandleToFree = block_getBlockHandle(pPool, pBlockToFree);
   
   /*
   Mark the freed memory as not used.  This allows a 
   quick visual check when debugging.
   */
   memset(pBlockToFree, NOT_USED, pHeader->blockSize);
   
   /*
   Place the freed memory at the head of the 
   free list.
   */
   pHandleToFree->pNext = pHeader->pCurrentFree;
   pHeader->pCurrentFree = pHandleToFree;
}


BlockHandle* block_getBlockHandle(void *pPool, void *pBlock)
{
  /*
  Since we know the blocks and their block handles are fixed
  relative to each other, if we can work out which Block we
  have (by effectively calculating which element it would
  be if the Blocks were an array) we can get the (address of)
  its associated BlockHandle.
  
  For example, if we calculate the supplied block is the fifth
  Block in the allocator we can use that value to index into the
  array of BlockHandles, and return the address of that BlockHandle
  structure.
  */
  PoolHeader *pHeader = (PoolHeader*)pPool;
  unsigned int blockStart = (unsigned int)pHeader->pBlockStart;
  unsigned int thisBlock = (unsigned int)pBlock;
  unsigned int blockIndex = (thisBlock - blockStart)/pHeader->blockSize;
  
  return &(pHeader->BlockHandles[blockIndex]);
  
}

unsigned int wordAlignBlock (const unsigned int blockSize)
{
  /*
  It's possible the block size may not align on an integer boundary
  (that is, may be an odd number of bytes; or only divisible by 2)
  We want to make sure we don't get odd word boundary alignments, so
  (over)allocate the block size to the nearest integer word boundary
  */
  return (blockSize + (blockSize % WORD_SIZE));
}

          
unsigned int calculateNumBlocks (
                                 const unsigned int poolSize, 
                                 const unsigned int blockSize, 
                                 const unsigned int maxBlocks
                                )
{
  /*
  Calculate how many actual (data) blocks can be allocated from the pool.
  
  The pool will always have to hold the pool header (even if no blocks are
  allocated), so we must subtract the size of the pool header struct from
  the supplied pool size to get the actual pool size.
  
  The actual size of each allocated block is effectively the requested size
  plus the size of its associated BlockHandle.

  */
  unsigned int actualPoolSize = 0;
  unsigned int actualBlockSize = 0;
  unsigned int actualNumBlocks = 0;
  
  if (poolSize < sizeof(PoolHeader))
  {
    /* Give up now.  We can't even allocate a header! */
    return 0;
  }
  actualPoolSize = poolSize - sizeof(PoolHeader);
  actualBlockSize = sizeof(BlockHandle) + blockSize;
  actualNumBlocks = actualPoolSize / actualBlockSize;
  
  /* 
  If the requested number of blocks is less than
  will actually fit into the pool, then return
  than number; otherwise use our calculated maximum
  */
  return (actualNumBlocks < maxBlocks)? actualNumBlocks : maxBlocks;

}
          
