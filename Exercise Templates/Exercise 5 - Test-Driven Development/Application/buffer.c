// Buffer.c

#include "buffer.h"

// --------------------------------------------------------------
// NOTE:
//
// The development process should be:
//
// 1 - Write a (new) test - that fails
// 2 - Implement the code to pass the test
// 3 - Re-factor the code (and re-test)
// 4 - Repeat until no more tests
//
// --------------------------------------------------------------
