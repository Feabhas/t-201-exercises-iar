#ifndef BUTTONS_H
#define BUTTONS_H

// Door button functions
//
void doorInit (void);
int doorOpen (void);

// Programme Switches
//
void psInit (void);
int psGetValue (void);

#endif // BUTTONS_H