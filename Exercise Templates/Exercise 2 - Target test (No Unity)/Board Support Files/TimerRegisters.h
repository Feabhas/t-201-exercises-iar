/************************************************************************ 
    Register definitions for LPC2129 Timer0

*************************************************************************/
#ifndef TIMERREGISTERS_H
#define TIMERREGISTERS_H

/* Register definitions */

#define T0IR    (*((volatile unsigned long *) 0xE0004000))
#define T0TCR   (*((volatile unsigned long *) 0xE0004004))
#define T0TC    (*((volatile unsigned long *) 0xE0004008))
#define T0PR    (*((volatile unsigned long *) 0xE000400C))
#define T0PC    (*((volatile unsigned long *) 0xE0004010))
#define T0MCR   (*((volatile unsigned long *) 0xE0004014))
#define T0MR0   (*((volatile unsigned long *) 0xE0004018))
#define T0MR1   (*((volatile unsigned long *) 0xE000401C))
#define T0MR2   (*((volatile unsigned long *) 0xE0004020))
#define T0MR3   (*((volatile unsigned long *) 0xE0004024))
#define T0CCR   (*((volatile unsigned long *) 0xE0004028))
#define T0CR0   (*((volatile unsigned long *) 0xE000402C))
#define T0CR1   (*((volatile unsigned long *) 0xE0004030))
#define T0CR2   (*((volatile unsigned long *) 0xE0004034))
#define T0CR3   (*((volatile unsigned long *) 0xE0004038))
#define T0EMR   (*((volatile unsigned long *) 0xE000403C))

/* Configuration enumerations */

enum Activation
{
  STOP,
  START
};

enum TimeoutAction
{
  INTERRUPT = 0x01,
  RESET     = 0x02
};

enum Interrupt
{
  ACKNOWLEDGE_INTERRUPT = 1
};

#endif /* TIMERREGISTERS_H */

