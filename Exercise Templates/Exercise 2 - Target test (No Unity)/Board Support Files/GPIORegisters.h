/************************************************************************ 
    Register definitions for LPC2129 GPIO

*************************************************************************/
#ifndef GPIOREGISTERS_H
#define GPIORREGISTERS_H

#define PINSEL0 (*((volatile unsigned long *) 0xE002C000))
#define PINSEL1 (*((volatile unsigned long *) 0xE002C004))

#define IO0PIN  (*((volatile unsigned long *) 0xE0028000))
#define IO0SET  (*((volatile unsigned long *) 0xE0028004))
#define IO0DIR  (*((volatile unsigned long *) 0xE0028008))
#define IO0CLR  (*((volatile unsigned long *) 0xE002800C))

#define IO1PIN  (*((volatile unsigned long *) 0xE0028010))
#define IO1SET  (*((volatile unsigned long *) 0xE0028014))
#define IO1DIR  (*((volatile unsigned long *) 0xE0028018))
#define IO1CLR  (*((volatile unsigned long *) 0xE002801C))

enum bits 
{ 
  bit16 = (1 << 16), 
  bit17 = (1 << 17), 
  bit18 = (1 << 18), 
  bit19 = (1 << 19),
  bit20 = (1 << 20),
  bit21 = (1 << 21),
  bit22 = (1 << 22),
  bit23 = (1 << 23),
  allPins = bit16|bit17|bit18|bit19|bit20|bit21|bit22|bit23
};

enum LEDs 
{
  LED1 = bit16, 
  LED2 = bit17, 
  LED3 = bit18, 
  LED4 = bit19
};




#endif /* GPIOREGISTERS_H */


