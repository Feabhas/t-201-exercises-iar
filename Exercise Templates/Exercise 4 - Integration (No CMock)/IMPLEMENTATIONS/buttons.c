#include "buttons.h"
#include "GPIORegisters.h"

#define BUTTON_LATCH_MASK (bit22)
#define DOOR_CLOSED_MASK  (bit16)
#define PS1_MASK          (bit17)
#define PS2_MASK          (bit18)
#define PS3_MASK          (bit19)
#define CANCEL_MASK       (bit20)
#define ACCEPT_MASK       (bit21)


void doorInit(void)
{
  IO0DIR &= ~DOOR_CLOSED_MASK;
}

int doorOpen(void)
{
  return(IO0PIN & DOOR_CLOSED_MASK) ? 0 : 1;
}

void psInit (void)
{
  IO0DIR &= ~(ACCEPT_MASK | CANCEL_MASK | PS1_MASK | PS2_MASK | PS3_MASK);
  IO1DIR |= BUTTON_LATCH_MASK;
  IO1CLR = BUTTON_LATCH_MASK;  
}

#ifdef NO_BUGS
  #define SET_AS !=
#else
  #define SET_AS = 
#endif

int psGetValue(void)
{
  unsigned int val = 0;
  
  IO1SET = BUTTON_LATCH_MASK;
  
  // Repeat until Accept button pressed
  //
  while (!(IO0PIN & ACCEPT_MASK))
  {
#ifdef NO_BUGS
    if((IO0PIN & PS1_MASK) != 0) val |= 0x01;
    if((IO0PIN & PS2_MASK) != 0) val |= 0x02;
    if((IO0PIN & PS3_MASK) != 0) val |= 0x04;
#else
    if((IO0PIN & PS1_MASK) != 0) val = 0x01;
    if((IO0PIN & PS2_MASK) != 0) val = 0x02;
    if((IO0PIN & PS3_MASK) != 0) val = 0x04; 
#endif

   if (IO0PIN & CANCEL_MASK)
   {
      IO1CLR = BUTTON_LATCH_MASK;
      IO1SET = BUTTON_LATCH_MASK;
      val = 0;
   }
  }
  
  IO1CLR = BUTTON_LATCH_MASK;
  while((IO0PIN & ACCEPT_MASK) != 0)
  {
  }
  
  return val;
}