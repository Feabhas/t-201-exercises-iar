#include "motor.h"
#include "GPIORegisters.h"

#define MOTOR_CTRL_MASK   (bit20)
#define MOTOR_DIRN_MASK   (bit21)

void motorInit(void)
{
  IO1DIR |= MOTOR_CTRL_MASK | MOTOR_DIRN_MASK;
  motorOff();
}

void motorOn(void)
{
  IO1SET = MOTOR_CTRL_MASK;
}

void motorOff(void)
{
  IO1CLR = MOTOR_CTRL_MASK;
}

void motorChangeDir(void)
{
  unsigned long val = IO1PIN;

#ifdef NO_BUGS
  val ^= MOTOR_DIRN_MASK;
#else
  val |= MOTOR_DIRN_MASK;
#endif
  
  IO1SET = val;
  IO1CLR = ~val;
}