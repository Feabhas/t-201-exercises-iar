/************************************************************************ 
    Register definitions for ARM Vectored Interrupt Controller (VIC)

*************************************************************************/
#ifndef VICREGISTERS_H
#define VICREGISTERS_H

#define VICDefVectAddr (*((volatile unsigned long *) 0xFFFFF034))
#define VICIntSelect   (*((volatile unsigned long *) 0xFFFFF00C))
#define VICIntEnable   (*((volatile unsigned long *) 0xFFFFF010))
#define VICIntEnClr    (*((volatile unsigned long *) 0xFFFFF014))
#define VICVectAddr    (*((volatile unsigned long *) 0xFFFFF030))
#define VICVectAddr0   (*((volatile unsigned long *) 0xFFFFF100))
#define VICVectCntl0   (*((volatile unsigned long *) 0xFFFFF200))

#endif /* VICREGISTERS_H */

