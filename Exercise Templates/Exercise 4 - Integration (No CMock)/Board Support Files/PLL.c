#include <NXP/iolpc2129.h>
#include "PLL.h"

#define OSCILLATOR_CLOCK_FREQUENCY  12000000  // 12Mhz
#define ARM_CORE_CLOCK_FREQUENCY    60000000  // 60Mhz
#define VALUE_OF_M                  (ARM_CORE_CLOCK_FREQUENCY/OSCILLATOR_CLOCK_FREQUENCY)-1            //implies PLLCFG_bit.MSEL=4            
#define PCLK                        (ARM_CORE_CLOCK_FREQUENCY/4)         //implies APBDIV_bit.APBDIV=0;
#define PLOCK                       0x0400

void initPLL(void)
{
  PLLCFG_bit.MSEL = VALUE_OF_M;       // M=4(5) CCLK = 60Mhz (M * 12Mhz)
  PLLCFG_bit.PSEL=1;                  // P=1(2) PLLfreq = 240Mhz (P * cclk * 2)
  
  PLLCON_bit.PLLE=1;                  // Enable the PLL
  PLLFEED=0xAA;                       // Feed PLL
  PLLFEED=0x55;                       // Feed PLL
  
  while(!(PLLSTAT & PLOCK)) {
    ;          // Wait for PLL to lock
  }
  
  PLLCON_bit.PLLC=1;                  // Connect the PLL
  PLLFEED=0xAA;                       // Feed PLL
  PLLFEED=0x55;                       // Feed PLL
  
  // Init Peripherial clock
  APBDIV_bit.APBDIV=0;      //0: pclk = cclk/4 (e.g. 60MHz/4 = 15Mhz)
//  APBDIV_bit.APBDIV=1;    //1: pclk = cclk/1 (e.g. 60MHz)
//  APBDIV_bit.APBDIV=2;    //2: pclk = cclk/2 (e.g. 60MHz/2 = 30Mhz)
}

uint32_t getprocessorClockFreq(void)
{
  uint32_t cclk = OSCILLATOR_CLOCK_FREQUENCY * (PLLCON & 3 ? (PLLCFG & 0x1F) + 1 : 1);
  return cclk;
}

uint32_t getperipheralClockFreq(void)
{
  uint32_t divider;
  switch (APBDIV_bit.APBDIV)
  {
  case 0:
    divider = 4;  // pclk = cclk/4
    break;
  case 1:
    divider = 1;  // pclk = cclk
    break;
  case 2:
    divider = 2;  // pclk = cclk/2
    break;
  default:
    divider = 1;  // pclk = cclk
    break;
  }
  return getprocessorClockFreq() / divider;
}
